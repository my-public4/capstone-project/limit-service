package com.bank.limit.service;

import com.bank.limit.entity.ProductEventEntity;
import com.bank.limit.entity.ProductLimitConfigEntity;
import com.bank.limit.repository.ProductLimitConfigEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProductLimitConfigService {
    @Autowired
    private ProductLimitConfigEntityRepository productLimitConfigEntityRepository;

    @Transactional
    public void updateLimit(ProductEventEntity event) {
        Instant updatedTime = Instant.now();
        Optional<ProductLimitConfigEntity> entityOptional
                = productLimitConfigEntityRepository.findById(event.getProductId());
        ProductLimitConfigEntity entity = null;
        if (entityOptional.isPresent()) {
            entity = entityOptional.get();
            entity.setDailyLimit(event.getProductLimitAmount());
            log.info("update ProductLimitConfigEntity " + entity.getProductCode() + " daily limit " + entity.getDailyLimit());
        } else {
            entity = new ProductLimitConfigEntity();
            entity.setProductCode(event.getProductCode());
            entity.setId(event.getProductId());
            entity.setDailyLimit(event.getProductLimitAmount());
            log.info("update ProductLimitConfigEntity " + entity.getProductCode() + " daily limit " + entity.getDailyLimit());
        }
        entity.setUpdatedByEventId(event.getEventId());
        entity.setUpdatedAt(updatedTime);
        productLimitConfigEntityRepository.save(entity);
    }

    public List<ProductLimitConfigEntity> retrieveProductLimits() {
        return productLimitConfigEntityRepository.findAll(
                Sort.by(Sort.Direction.DESC, "updatedAt")
        );
    }

    public Optional<ProductLimitConfigEntity> retrieveProductLimit(String productCode) {
        return productLimitConfigEntityRepository.findProductLimitConfigEntityByProductCode(productCode);
    }
}
