package com.bank.limit.service;

import com.bank.limit.entity.DailyTransactionEntity;
import com.bank.limit.entity.ProductLimitConfigEntity;
import com.bank.limit.event.TransactionLimitEvent;
import com.bank.limit.model.ReservedLimitResponse;
import com.bank.limit.model.TransferRequest;
import com.bank.limit.model.UserLimitRequest;
import com.bank.limit.model.UserLimitResponse;
import com.bank.limit.repository.DailyTransactionEntityRepository;
import com.bank.limit.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Stream;

@Slf4j
@Service
public class UserLimitService {
    @Value("${camel.lra.coordinator-url}")
    private String SAGA_URL;
    private static String SAGA_PATH = "/lra-coordinator/";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${limit.kafka.topic.transaction-limit}")
    private String TRANSACTION_LIMIT_TOPIC;

    @Autowired
    private DailyTransactionEntityRepository dailyTransactionEntityRepository;
    @Autowired
    private ProductLimitConfigService productLimitConfigService;

    @Transactional
    public UserLimitResponse retrieveUserLimitTxn(UserLimitRequest request) {
        Optional<ProductLimitConfigEntity> productLimitConfigEntityOptional =
                productLimitConfigService.retrieveProductLimit(request.getProductCode());
        UserLimitResponse response = new UserLimitResponse();
        if (productLimitConfigEntityOptional.isPresent()) {
            response.setProductLimitConfig(productLimitConfigEntityOptional.get());
        }
        List<DailyTransactionEntity> txnList = retrieveUserLimitTxn(
                request.getUser(), request.getDate());
        log.info("txnList transaction to calculate: " + txnList.size());
        BigDecimal accumulatedDailyAmount = BigDecimal.ZERO;
        for (DailyTransactionEntity txt : txnList) {
            accumulatedDailyAmount = accumulatedDailyAmount.add(txt.getAmount());
        }
        response.setAccumulatedDailyAmount(accumulatedDailyAmount);
        return response;
    }

    @Transactional
    public List<DailyTransactionEntity> retrieveUserLimitTxn(String userId, Date date) {
        List<DailyTransactionEntity> txnList;
        HashMap<UUID, DailyTransactionEntity> mapTxnList = new HashMap<>();
        try (Stream<DailyTransactionEntity> stream = dailyTransactionEntityRepository
                .findByFromCifAndCreatedAtAfterOrderByCreatedAt(
                        userId, atStartOfDay(date))) {
            txnList = stream.toList();
        }
        log.info("Found transaction: " + txnList.size());
        for (DailyTransactionEntity txn : txnList) {
            if (!mapTxnList.containsKey(txn.getTransactionId())) {
                mapTxnList.put(txn.getTransactionId(), txn);
            } else {
                DailyTransactionEntity alreadyAddedTxt = mapTxnList.get(txn.getTransactionId());
                log.info("Transaction exist/duplicated with id: " + txn.getTransactionId());
                log.info(" - " + txn.toString());
                log.info(" - " + alreadyAddedTxt.toString());
                if (!txn.getStatus().equalsIgnoreCase(alreadyAddedTxt.getStatus())) {
                    mapTxnList.remove(txn.getTransactionId());
                    log.info(" - With different status --> this is a reserved and reverted transactions");
                } else {
                    log.info(" - With same status --> ignored");
                }
            }
        }
        return mapTxnList.values().stream().toList();
    }

    @Transactional
    public String reserveLimit(@Header(value = "Long-Running-Action") String sagaId,
                               @Body String request) {
        sagaId = getSagaId(sagaId);
        log.info("reserve limit for transaction " + request);
        log.info("with sagaID " + sagaId);

        TransferRequest transferRequest = (TransferRequest) ObjectUtil.stringToObject(request, TransferRequest.class);
        //create daily transaction
        DailyTransactionEntity dailyTransactionEntity = new DailyTransactionEntity();
        dailyTransactionEntity.setAmount(transferRequest.getAmount());
        dailyTransactionEntity.setTransactionId(transferRequest.getClientTransactionId());
        dailyTransactionEntity.setBeneficiary(transferRequest.getBeneficiary());
        dailyTransactionEntity.setId(UUID.randomUUID());
        dailyTransactionEntity.setFromCif(transferRequest.getFromCif());
        dailyTransactionEntity.setProductCode(transferRequest.getProductCode());
        dailyTransactionEntity.setProductId(transferRequest.getProductId().toString());
        dailyTransactionEntity.setCreatedAt(Instant.now());
        dailyTransactionEntity.setStatus("RESERVED");
        dailyTransactionEntity.setSagaId(sagaId);
        dailyTransactionEntityRepository.save(dailyTransactionEntity);
        log.info("+ Insert transaction: " + ObjectUtil.objectToString(dailyTransactionEntity));
        UserLimitRequest limitRequest = new UserLimitRequest();
        limitRequest.setUser(transferRequest.getFromCif());
        limitRequest.setProductCode(transferRequest.getProductCode());
        limitRequest.setDate(new Date());
        UserLimitResponse userLimitResponse = retrieveUserLimitTxn(limitRequest);

        ReservedLimitResponse response = new ReservedLimitResponse();
        response.setUserLimitResponse(userLimitResponse);
        response.setTransferRequest(transferRequest);
        log.info("response user-limit " + ObjectUtil.objectToString(response));

        TransactionLimitEvent transactionLimitEvent = new TransactionLimitEvent();
        transactionLimitEvent.setClientTransactionId(transferRequest.getClientTransactionId());
        transactionLimitEvent.setSagaId(sagaId);
        transactionLimitEvent.setCheckedTime(Instant.now());
        transactionLimitEvent.setAccumulatedMonthlyAmount(userLimitResponse.getAccumulatedMonthlyAmount());
        transactionLimitEvent.setAccumulatedDailyAmount(userLimitResponse.getAccumulatedDailyAmount());
        notifyLimitEvent(transactionLimitEvent);

        return ObjectUtil.objectToString(response);
    }

    @Transactional
    public String rollbackLimit(@Header("Long-Running-Action") String sagaId) {
        log.info(" rollback for saga: " + sagaId);
        sagaId = getSagaId(sagaId);
        Optional<DailyTransactionEntity> dailyTransactionEntityOptional = dailyTransactionEntityRepository.findBySagaId(sagaId);
        if (!dailyTransactionEntityOptional.isPresent()) {
            return null;
        }
        DailyTransactionEntity oldEntity = dailyTransactionEntityOptional.get();
        DailyTransactionEntity newEntity = new DailyTransactionEntity();
        newEntity.setStatus("REVERSED");
        newEntity.setCreatedAt(Instant.now());
        newEntity.setProductCode(oldEntity.getProductCode());
        newEntity.setProductId(oldEntity.getProductId());
        newEntity.setId(UUID.randomUUID());
        newEntity.setAmount(oldEntity.getAmount());
        newEntity.setBeneficiary(oldEntity.getBeneficiary());
        newEntity.setFromCif(oldEntity.getFromCif());
        newEntity.setTransactionId(oldEntity.getTransactionId());
        newEntity.setSagaId(sagaId);
        log.info("+ Rollback transaction: " + ObjectUtil.objectToString(newEntity));
        dailyTransactionEntityRepository.save(newEntity);
        return ObjectUtil.objectToString(newEntity);
    }

    public static Instant atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    private static Instant localDateTimeToDate(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant();
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private String getSagaId(String sagaPath) {
        return sagaPath.toUpperCase(Locale.ROOT).replace(SAGA_URL.toUpperCase(Locale.ROOT)
                + SAGA_PATH.toUpperCase(Locale.ROOT), "");
    }

    public void notifyLimitEvent(TransactionLimitEvent event) {
        String eventStr = ObjectUtil.objectToString(event);
        log.info(" send limit status to Topic : " + TRANSACTION_LIMIT_TOPIC + " event: " + eventStr);
        kafkaTemplate.send(TRANSACTION_LIMIT_TOPIC, eventStr);
    }
}
