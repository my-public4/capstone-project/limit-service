package com.bank.limit.repository;

import com.bank.limit.entity.ProductEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductEventEntityRepository extends JpaRepository<ProductEventEntity, UUID> {
}